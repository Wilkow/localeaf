# Localeaf, local containerized environment for working with latex documents.
## File structure
- `.devcontainer/devcontainer.json` - Container configuration, add any new VSCode extensions/settings there
- `.devcontainer/Dockerfile` - Container configuration, add any needed system packages there
- `doc/` - Directory for latex source files
- `doc/out/` - Gitignored directory which holds all latex temporary files and resulting pdf document
